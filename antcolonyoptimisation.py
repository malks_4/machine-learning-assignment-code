import numpy as np
import pandas as pd

def state_distance(s1,s2):
    return np.sqrt(np.abs((s1['x']**2)-(s2['x']**2)) + np.abs((s1['y']**2)-(s2['y']**2))) #calculates euclidian dist between 2 states

def all_states_distance(solution,distance_matrix):  #distance for a whole route
    dist = 0
    for i in range(len(solution)-1):
        if i == (len(solution)-1):
            dist +=  distance_matrix[solution[i]-1][solution[0]-1] #distance to go back to starting point
        else:
            dist +=  distance_matrix[solution[i]-1][solution[i+1]-1] #distance between 2 consecutive states
    return dist

def get_distance_matrix(coords_df): #holds all distances between all possible moves an ant can make
    distance_list = []
    for i in range(len(coords_df.index)):
        distances_ij = []
        for j in range(len(coords_df.index)):
            distance = state_distance(coords_df.iloc[i],coords_df.iloc[j])
            distances_ij.append(distance)
        distance_list.append(distances_ij)
        distance_matrix = np.asarray(distance_list)
    return distance_matrix

def get_pheromone_matrix(coords_df): #fills the pheromone matrix with values of 1 and returns it
    pheromone_list = []
    for i in range(len(coords_df.index)):
        pheromones_ij = []
        for j in range(len(coords_df.index)):
            pheromones_ij.append(1)
        pheromone_list.append(pheromones_ij)
        pheromone_matrix = np.asarray(pheromone_list)
    return pheromone_matrix        


def update_pheromone_matrix(pheromone_matrix, dist_each_ant ,evaporation_rate,Q,solutions):
    pheromone_matrix = pheromone_matrix*evaporation_rate #pheromone evaporation
    for i in range(len(solutions)-1): #updating pheromone levels
        for j in range(len(solutions[i])-1):
            if i == (len(solutions)-1):
                pheromone_matrix[solutions[i][j]-1][solutions[i][0]-1] += Q/dist_each_ant[i] 
            else:
                pheromone_matrix[solutions[i][j]-1][solutions[i][j+1]-1] += Q/dist_each_ant[i]

def move_ant(allowed_list,solution,distance_matrix,pheromone_matrix,alpha,beta): #probabbilistically choosing where the ant should move next
    i = solution[len(solution)-1]
    denominator = 0
    for j in allowed_list: #summation (denominator)
        denominator += (pheromone_matrix[i-1][j-1]**alpha)*((1/distance_matrix[i-1][j-1])**beta) #summation of pheromone*visibility
    p_ij = []
    for j in allowed_list:
        probability = (pheromone_matrix[i-1][j-1]**alpha)*((1/distance_matrix[i-1][j-1])**beta)/denominator
        p_ij.append(probability)

    move_to = np.random.choice(allowed_list, p=p_ij)
    solution.append(move_to)
    allowed_list.remove(move_to)

def aco(no_of_ants, iterations, coords_df, distance_matrix, pheromone_matrix, evaporation_rate,Q,alpha,beta): #aco algorithm
    last_solutions = []
    for i in range(1,iterations):
        solutions = [] #all ant solutions for this iteration (used to update pheromone matrix)
        dist_each_ant = []  #hold distance each ant travelled
        for ant in range(1,no_of_ants):
            solution = []
            allowed_list = coords_df['index'].tolist()
            starting_point = np.random.choice(allowed_list) #set random starting point
            solution.append(starting_point)
            allowed_list.remove(starting_point)
            while len(allowed_list) > 0:
                move_ant(allowed_list,solution,distance_matrix,pheromone_matrix,alpha,beta)
            solution.append(starting_point)    
            dist_each_ant.append(all_states_distance(solution,distance_matrix)) #calculates distance that this particular ant travelled
            print("Distance: "+str(all_states_distance(solution,distance_matrix))) #to see what is happening
            solutions.append(solution)
        update_pheromone_matrix(pheromone_matrix,dist_each_ant,evaporation_rate,Q,solutions) #updating pheromone levels
        if i == iterations-1:
            last_solutions = solutions #this is done in order to return the best ant of the last iteration

    #remaining code finds the best ant of the last iteration
    best_solution = last_solutions[0] 
    best_solution_distance = all_states_distance(last_solutions[0],distance_matrix)
    for i in range(1,len(solutions)):
        current_distance = all_states_distance(last_solutions[i],distance_matrix)
        if current_distance < best_solution_distance:
            best_solution_distance = current_distance
            best_solution = last_solutions[i]
    return best_solution #returns solution of the best ant of the final iteration           


def main():
    coords_df = pd.read_csv("USstates.txt", sep=' ', names = ['index','x','y']) #reads textfile

    distance_matrix = get_distance_matrix(coords_df) #distance matrix between all cities
    pheromone_matrix = get_pheromone_matrix(coords_df)
    
    no_of_ants = 10
    iterations = 1000
    evaporation_rate = 0.8
    Q = 100000
    alpha = 100
    beta = 1

    solution = aco(no_of_ants,iterations,coords_df,distance_matrix,pheromone_matrix,evaporation_rate,Q,alpha,beta)

    print(solution)
    print("Final Distance: "+str(all_states_distance(solution,distance_matrix)))


if __name__ == '__main__':
    main()